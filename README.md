Phần mềm chấm công là ứng dụng cần thiết hỗ trợ các công ty, doanh nghiệp trích xuất dữ liệu từ máy chấm công của nhân viên một cách đầy đủ và chính xác nhất. Hiện nay trên thị trường có rất nhiều phần mềm chấm công, chấm công với nhiều chức năng khác nhau. Điều đáng nói là có 2 phiên bản phổ biến của phần mềm điều chỉnh thời gian Wise eye là 3.9 và 5.1. Bài viết hôm nay sẽ giới thiệu đến bạn đọc phần mềm chỉnh thời gian Wise eye On 3.9 và cách sử dụng.
 <img src="https://i.imgur.com/g3UXGdF.jpg">

##Giới thiệu 
Như đã nói ở trên, phần mềm chấm công Wise eye là một phần mềm rất đáng tin cậy và chất lượng cao tại Việt Nam. Phiên bản Wise eye On 3.9 thuộc hệ thống phần mềm đa dạng và thông minh này. Đây là phần mềm được lựa chọn hàng đầu, có rất nhiều lượt tải trên thị trường vì nó không quá khó sử dụng. Phần mềm được xây dựng trên nền tảng net 2.0 của Microsoft, tốc độ xử lý số liệu chính xác, hệ thống báo cáo đa dạng phù hợp với người Việt Nam. Chính vì vậy, Wise eye On 3.9 luôn được người dùng hài lòng và đánh giá cao.
##Tiện ích
Phần mềm đếm thời gian Wise eye On 39 có nhiều chức năng gần như hoàn hảo mang đến cho người dùng trải nghiệm tốt nhất.
Sử dụng Wise eye trên 3.9, các vấn đề như: thông tin báo cáo, phân công lịch làm việc, xuất báo cáo hay chỉnh sửa thời gian sẽ được xử lý nhanh chóng.
Không cần nhiều phương tiện phức tạp, chỉ cần sử dụng hai hệ quản trị cơ sở dữ liệu Access và SQL là bạn có thể cài đặt phần mềm một cách nhanh chóng và dễ dàng.
Nhờ thiết kế được cập nhật liên tục và độ ổn định cao, Wise Eye On 39 chronograph hiếm khi bị lỗi kết nối.
Giao diện tiếng Việt đơn giản, dễ nhìn giúp người dùng sử dụng tốt hơn.
Phần mềm tương thích với hầu hết các hệ điều hành Windows hiện nay nên bạn có thể cài đặt chỉ với vài cú nhấp chuột.
Xem Video: https://www.youtube.com/watch?v=9h3OXVVQ8S8
##Cách sử dụng phần mềm máy công công Wise eye On 3.9
Sau khi cài đặt thành công phần mềm máy chấm công Wise eye phiên bản 3.9 trên máy tính. Bạn thực hiện các bước sau để sử dụng phần mềm:
Khai báo máy chấm công.
Khi màn hình hẹn giờ xuất hiện
Kiểm tra địa chỉ IP của phần mềm và địa chỉ IP trên máy, chúng phải trùng khớp và phải thuộc IP LAN của công ty.
Bước tiếp theo bạn nhập mã số đăng ký của máy chấm công Wise eye vào phần mềm.
Tải dữ liệu về máy tính
Khai báo vân tay, mật khẩu, thẻ nhớ của nhân viên và tải dữ liệu về máy tính trên máy chấm công: chọn máy chấm công> bấm Tải nhân viên> Duyệt nhân viên> Chọn nhân viên> Thực hiện cập nhật.
###Khai báo cho phòng ban, nhân viên
Thực hiện lần lượt:
Chọn Dữ liệu> Thông tin Công ty.
Dữ liệu> Chọn Tuyên bố Tiêu đề.
Chọn Dữ liệu> Theo bộ phận báo cáo.
Chọn Dữ liệu> Phòng ban liên quan> Thêm các phòng ban nhỏ hơn.
Quay lại tab "Dữ liệu"> chọn tab "Quản lý nhân viên"> điền toàn bộ danh sách, sau đó tiếp tục sửa tên nhân viên> nhấn "Cập nhật" để hoàn tất quá trình.
###Cập nhật thông tin lên máy chấm công
Chọn tab Chấm công> tìm và chọn tải nhân viên lên máy chấm công> chọn mục phòng ban> nhấn vào nhân viên để chọn> chọn chuyển xuống> cuối cùng nhấn vào tải lên máy chấm công.
##Khai báo lịch làm việc cho nhân viên.
Bấm Cài đặt Định giờ> Chọn Khai báo theo ca> Nhập thông tin bên cạnh giờ làm việc của từng ca.
Sau đó quay lại Cài đặt Chấm công> bấm vào báo cáo lịch làm việc, điều chỉnh theo yêu cầu của công ty. Tất cả các nhãn thông tin đều bằng tiếng Việt nên rất dễ hiểu và dễ sử dụng.
##Xuất dữ liệu chấm công
Bước này được hoàn thành vào cuối tháng hoặc vào cuối tuần, tùy thuộc vào cơ sở giáo dục. Đầu tiên chúng ta cần tải dữ liệu từ máy chấm công về máy tính, sau đó kết xuất báo cáo và tổng hợp.
Chọn thời gian> bấm để tải dữ liệu thời gian> thông tin sẽ hiển thị trên màn hình: chọn ngày xuất> bấm duyệt từ định thời và đợi máy cập nhật.
Tiếp theo, bạn chọn Chấm công và Báo cáo> Chọn Giờ theo ca> Phòng ban> Nhân viên> Xem ngày giờ> Xuất tệp báo cáo.
Hy vọng bài viết hướng dẫn sử dụng phần mềm định thời Wise eye On 3.9 trên đây đã phần nào giúp các bạn có thêm những thông tin hữu ích về cách sử dụng phần mềm định thời Wise eye On 3.9.
Nguồn: https://thietbimaychamcong.com/cai-dat-may-cham-cong/
Xem thêm:
https://sites.google.com/view/thietbimaychamcong-tbmcc/blog/thietbimaychamcong-tbmcc/nh%E1%BB%AFng-c%C3%A1ch-hack-m%C3%A1y-ch%E1%BA%A5m-c%C3%B4ng-%C4%91%C6%A1n-gi%E1%BA%A3n-nh%E1%BA%A5t
https://thietbimaychamcongcom.blogspot.com/2021/11/phan-mem-may-cham-cong.html
https://thietbimaychamcong.wordpress.com/2021/11/13/cung-tim-hieu-may-cham-cong-tieng-anh-la-gi/
https://thietbimaychamcong.wixsite.com/home/post/t%E1%BB%95ng-quan-v%E1%BB%81-tool-l%E1%BA%A5y-s%E1%BB%91-%C4%91%C4%83ng-k%C3%BD-m%C3%A1y-ch%E1%BA%A5m-c%C3%B4ng
https://medium.com/@tbmcc/h%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-c%C3%A1ch-s%E1%BB%ADa-d%E1%BB%AF-li%E1%BB%87u-tr%C3%AAn-m%C3%A1y-ch%E1%BA%A5m-c%C3%B4ng-nhanh-ch%C3%B3ng-c3e3a83dcd65
https://dribbble.com/shots/16852162-NH-NG-CA-CH-K-T-N-I-MA-Y-CH-M-C-NG-V-I-MA-Y-TI-NH-T-T-NH-T?new_shot_upload=true&utm_source=Clipboard_Shot&utm_campaign=TBMCC&utm_content=NH%E1%BB%AENG%20CA%CC%81CH%20K%C3%8A%CC%81T%20N%C3%94%CC%89I%20MA%CC%81Y%20CH%C3%82%CC%81M%20C%C3%94NG%20V%C6%A0%CC%81I%20MA%CC%81Y%20TI%CC%81NH%20T%E1%BB%90T%20NH%E1%BA%A4T&utm_medium=Social_Share&utm_source=Clipboard_Shot&utm_campaign=TBMCC&utm_content=NH%E1%BB%AENG%20CA%CC%81CH%20K%C3%8A%CC%81T%20N%C3%94%CC%89I%20MA%CC%81Y%20CH%C3%82%CC%81M%20C%C3%94NG%20V%C6%A0%CC%81I%20MA%CC%81Y%20TI%CC%81NH%20T%E1%BB%90T%20NH%E1%BA%A4T&utm_medium=Social_Share
https://www.liveinternet.ru/users/tbmcc/post488181485/
https://sway.office.com/ne1lJua6PHaRGBCx?ref=Link
https://www.goodreads.com/story/show/1375679-h-ng-d-n-c-i-t-m-y-ch-m-c-ng-chi-ti-t
https://gitlab.com/TBMCC/phan-mem-may-cham-cong/-/blob/main/README.md

Thông tin liên hệ [Thiết Bị Máy Chấm Công](https://thietbimaychamcong.com/) ThietBiMayChamCong TBMCC

Địa chỉ: 32 Đào Tấn, Phường Ngọc Khánh, Quận Ba Đình, Hà Nội

Hotline: 0934590833

Email: thietbimaychamcongcom@gmail.com

Website: https://thietbimaychamcong.com/

Fanpage: https://www.facebook.com/thietbimaychamcong/

Google Business: https://thiet-bi-may-cham-cong.business.site/

hashtag: #ThietBiMayChamCong #TBMCC #Thiết_Bị_Máy_Chấm_Công #Thiet_Bi_May_Cham_Cong #máy_chấm_công #may_cham_cong #maychamcong #máy_chấm_công_vân_tay #may_cham_cong_van_tay #maychamcongvantay #may_cham_cong_khuon_mat #máy_chấm_công_khuôn_mặt #maychamcongkhuonmat #máy_chấm_công_thẻ_từ #may_cham_cong_the_tu #maychamcongthetu #máy_chấm_công_thẻ_giấy #may_cham_cong_the_giay #maychamcongthegiay #thietbimaychamcongcom #thiết_bị_máy_chấm_công_com #thiet_bi_may_cham_cong_com
